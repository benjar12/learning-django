import datetime, os

from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Context
from django.shortcuts import render_to_response

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

def hello(request):
    return HttpResponse("hello world")

def time(request):
    now = datetime.datetime.now()
    t = get_template('time.html')
    html = t.render(Context({'current_date':now}))
    return HttpResponse(html)

def hours_ahead(request, offset):
    try:
        hour_offset = int(offset)
    except ValuError:
        raise Http404()
    next_time = datetime.datetime.now() + datetime.timedelta(hours=hour_offset)
    
    return render_to_response('hours_ahead.html', locals())