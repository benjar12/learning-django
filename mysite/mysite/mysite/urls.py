from django.conf.urls import patterns, include, url

from django.contrib import admin

from mysite.views import hello
from mysite.views import time
from mysite.views import hours_ahead

from books import views
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'mysite.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

                       url(r'^admin/', include(admin.site.urls)),
                       ('^hello/$',hello),
                       ('^time/$',time),
                       ('^time/plus/(\d{1,2})/$', hours_ahead),
                       (r'^search-form/$', views.search_form),
                       (r'^search/$', views.search),
                       (r'^contact/$', views.contact),
)
