from django.shortcuts import render
from django.shortcuts import render_to_response

from django.http import HttpResponse

from django.core.mail import send_mail
from django.http import HttpResponseRedirect

from books.models import Book

# Create your views here.
def search_form(request):
    
    return render_to_response('search_form.html')

def search(request):
    #holds any errors that we incounter.
    errors = []
    if 'q' in request.GET:
        q = request.GET['q']
        if not q:
            errors.append('Enter a search term.')
        elif len(q) > 20:
            errors.append('Enter at most 20 characters.')
        else:
            books = Book.objects.filter(title__icontains=q)  
            return render_to_response('search_results.html',{'books':books,'query':q})
    
    return render_to_response('search_form.html',{'errors':errors})

def contact(request):
    errors = []
    if request.method == 'POST':
        if not request.POST.get('subject', ''):
            errors.append('Enter a subject.')
        if not request.POST.get('message', ''):
            errors.append('Enter a message.')
        if request.POST.get('e-mail') and '@' not in request.POST['e-mail']:
            errors.append('Enter a valid e-mail.')
        if not errors:
            send_mail(request.POST['subject'],
                      request.POST['message'],
                      request.POST.get('e-mail', 'noreply@benjarman.com'),
                      ['benjarman@me.com']
                      )
            return HttpResponseRedirect('/contact/thanks/')
    return render_to_response('contact_form.html',{'errors':errors})